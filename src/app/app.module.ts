import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesService } from "./messages/messages.service";
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { NavigationComponent } from './navigation/navigation.component';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import { MessageComponent } from './messages/message/message.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { MessageFormComponent } from './messages/message-form/message-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    NavigationComponent,
    MessagesFormComponent,
    MessageComponent,
    MessageFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([ 
      {path:'',component:MessagesComponent}, 
      {path:'users', component:UsersComponent},
      {pathMatch:'full',path:'messagesForm/:key', component:MessagesFormComponent},      
      {pathMatch:'full',path: 'message/:id', component: MessageComponent},
      {pathMatch: 'full',path: 'message-form/:id', component: MessageFormComponent},      
    ])
  ],
  providers: [
  	MessagesService,
  	UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
