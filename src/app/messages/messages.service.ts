import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class MessagesService {
  http:Http;//http -> שם התכונה. Http-> סוג התכונה
  getMessages(){
      //return ['Message1','Message2','Message3','Message4'];
    //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get("http://localhost:8888/messages");

  }
  
  getMessage(id){
    return this.http.get('http://localhost:8888/messages/' + id);
  }

  postMessage(data) // דטה= נתונים של הטופס
  {
    let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
   let  params = new HttpParams().append('message', data.message); // פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
   return this.http.post('http://localhost:8888/messages', params.toString(),options);

  }

    putMessage(data,key){
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
   
        }
        let params = new HttpParams().append('message',data.message);
        return this.http.put('http://localhost:8888/messages/'+ key,params.toString(), options);
      }

  deleteMessage(key){
    return this.http.delete('http://localhost:8888/messages/'+key);
  }


  constructor(http:Http) {
    this.http = http;
   }

}